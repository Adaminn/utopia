﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtopiaAPI;

namespace UtopaServer
{
    public abstract class Tile
    {
     
        public int posX, posY;
        public int ID;
        public TileID type;

    }

    public class Field : Tile
    {
        public int food;
        public bool harvested = false;
        public int helpers = 0;
       

        public Field(int x, int y, int id, int food) 
        {
            this.posX = x;
            this.posY = y;
            this.ID = id;
            this.food = food;
            this.type = TileID.Field;
        }

    }
}
