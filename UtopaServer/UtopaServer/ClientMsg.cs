﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtopiaAPI;

namespace UtopaServer
{
    class ClientMsg
    {
        public NetMsg msg;
        public Player player;
        public ClientMsg(NetMsg m, Player p)
        {
            this.msg = m;
            this.player = p;
        }
    }
}
