﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using UtopiaAPI;

namespace UtopaServer
{
    class Player
    {
        public Socket socket;
        public Vector2 Position;
        public Vector2 Velocity;
        public Vector2 Direction = new Vector2(0,0);
        public Action action = new HarvestAction(ActionID.Nothing);

        public int ID;
        public string username;
        public string passwordHash;

        public float moveSpeed = 5f;
        public bool isDirty = false;
        public bool canMove = true;
        public bool metapacket = true;

        public int food;

       

        List<Item> inventory = new List<Item>();
        int health;

        public Player(Socket s)
        {
            this.socket = s;
            food = GameConstants.PLAYER_START_FOOD;
        }


    }
}
