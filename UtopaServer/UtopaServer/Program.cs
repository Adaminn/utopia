﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UtopiaAPI;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Diagnostics;



namespace UtopaServer
{
    public struct Vector2
    {
        public float X;
        public float Y;

        public Vector2(float x, float y)
        {
            X = x;
            Y = y;
        }

        public static implicit operator Vector2(UtopiaAPI.Vector2 other)
        {
            return new Vector2(other.X, other.Y);
        }
        public static implicit operator UtopiaAPI.Vector2(Vector2 other)
        {
            return new UtopiaAPI.Vector2(other.X, other.Y);
        }


    }
    class Program
    {

    
        const int DEFAULT_BYTE_SIZE = 1024;

        const int GAME_TICK = 1000 / 60;
        const int MAP_HEIGHT = 100;
        const int MAP_WIDTH = 100;
        const int PORT = 26000;

        static TileID[,] TileMap = new TileID[MAP_HEIGHT, MAP_WIDTH];

        private static Dictionary<int, Tile> tileDB = new Dictionary<int, Tile>();

        static List<Player> players;
        private static int lastID = 0;
        private static Queue<ClientMsg> queuedMessages = new Queue<ClientMsg>();

        public static Program Current;

        private static Stopwatch stopWatch;
        private static long deltaTime;
        private static int helpers = 0;
        private static bool gameStarted = false;
        private static DateTime gameStartedTime;
        private static DateTime lastEatTime;
      
        private static int eatAmmout = 0;


        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            players = new List<Player>();
            CreateMap();
            // var server = new Server();
            // server.Initialize();
            Initiliaze();



        }
        static void CreateMap()
        {           

            Random rnd = new Random();
         
            int id = 1;

            //generate background

            for (int y = 0; y < MAP_HEIGHT; y++)
            {
                for (int x = 0; x < MAP_WIDTH; x++)
                {
                    if (rnd.Next(8) == 0)
                    {
                      
                        TileMap[y, x] = TileID.Grass2;
                                             
                    }
                    else
                    {
                      
                        TileMap[y, x] = TileID.Grass1;
                    }

                }
            }

            //generate fileds
            {
                int x, y;
                // near center
                for (int i = 0; i < 7; i++)
                {
                  
                    x = rnd.Next(MAP_WIDTH / 4, MAP_WIDTH / 4 * 3);
                    y = rnd.Next(MAP_HEIGHT / 4, MAP_HEIGHT / 4 * 3);
                    if (TileMap[y, x] != TileID.Field)
                    {
                     
                        TileMap[y, x] = TileID.Field;
                        tileDB.Add(id, new Field(x, y, id, 100));
                        id++;


                    }
                    else
                    {

                        i--;


                    }
                }
                // far from center
                for (int i = 0; i < 3; i++)
                {
                
                    x = rnd.Next(3, 20);
                    y = rnd.Next(3, 20);
                    if (rnd.Next(0, 2) == 0) {

                        //flip x
                        x = MAP_WIDTH - x;
                        }
                    if (rnd.Next(0, 2) == 0)
                    {

                        //flip y
                        y = MAP_HEIGHT -y;
                    }
                    if (TileMap[y, x] != TileID.Field)
                    {
                       
                        TileMap[y, x] = TileID.Field;
                        tileDB.Add(id, new Field(x, y, id, 100));
                        id++;


                    }
                    else
                    {

                        i--;


                    }
                }
                TileMap[50, 55] = TileID.Field;
                tileDB.Add(id, new Field(55, 50, id, 100));
                id++;
                TileMap[49, 63] = TileID.Field;
                tileDB.Add(id, new Field(63, 49, id, 100));
                id++;
                foreach (Tile t in tileDB.Values)
                {
                    Console.WriteLine("tildeDB:  " + t.ID + "  type: "+ t.type+" posX " + t.posX + " posY " + t.posY);
                }
            }




        }
       
        static void Initiliaze()
        {
            var tcpListener = new TcpListener(IPAddress.Any,PORT);
      
            tcpListener.Start();
            Console.WriteLine("Server has started");


            new Thread(() =>
            {
                while (true)
                {
                    var clientSocket = tcpListener.AcceptSocket();
                    Thread listenThread = new Thread(new ParameterizedThreadStart(OnAccept));
                    listenThread.Start(clientSocket);

                }
            }).Start();

            /*
            {
                var bot = new Bot();
                bot.Start();
            }
            {
                var bot = new Bot();
                bot.Start();
            }
            

            */

            stopWatch = new Stopwatch();
            stopWatch.Start();


            Thread.Sleep(GAME_TICK);
         
            try {

                long delay = 0;
                int waitAction = 0;
                int waitSec = 0;
                int waitEatSec = 0;

               
                while (true)
                {
                    

                    ProccessMessages();                  
                    UpdatePositions();
                    if (waitAction == (int)GameConstants.ACTION_TIME_MILISEC/GAME_TICK) // perform actions
                    {
                        waitAction = 0;
                       
                        PerformActions();
                    }
                    if (waitSec == (int)999/GAME_TICK) // check if game ended
                    {
                      
                        waitSec = 0;

                        SecPassed(waitEatSec);
                    }


                    delay += stopWatch.ElapsedMilliseconds - GAME_TICK;  // catch up for the lost time (caused by delay of the two functions) by increasing delay for every extra miliseconds. Thread.Sleep will use this variable to know how long it should sleep
                    stopWatch.Restart();
                    //Thread.Sleep(Math.Max(GAME_TICK - (int)delay ,0));  
                    Thread.Sleep(GAME_TICK);
                    waitAction++;
                    waitSec++;



                }

            } catch (Exception e)
            {
                Console.WriteLine("Main thread error " + e.ToString());
            }

        }
        private static void ProccessMessages()
        {
            while (queuedMessages.Count > 0)
            {


                lock (queuedMessages)
                {
                    var message = queuedMessages.Dequeue();

                    lock (players)
                    {
                        ProcessMessage(message.msg, message.player);
                    }

                }


            }
        }
      
        private static void UpdatePositions()
        {
            
            lock (players)
            {

                foreach (Player player in players)
                {
                    if (player.action.actionID == ActionID.Nothing)
                    {
                        deltaTime = stopWatch.ElapsedMilliseconds;
                        player.Position.X = player.Position.X + player.Direction.X * player.moveSpeed * deltaTime / 1000;
                        player.Position.Y = player.Position.Y + player.Direction.Y * player.moveSpeed * deltaTime / 1000;
                    }

                    if (player.isDirty) // send only distinct coords
                    {

                        SMSG_PlayersNewCoordinates pc = new SMSG_PlayersNewCoordinates();
                        pc.Position.X = player.Position.X;
                        pc.Position.Y = player.Position.Y;
                        pc.Movement.X = player.Direction.X;
                        pc.Movement.Y = player.Direction.Y;
                        pc.id = player.ID;
                        
                        sendMsgToAllPlayers(pc);
                        player.isDirty = false;
                    }
                   

                }
            }

        }
        private static void PerformActions()
        {
           
         
            lock (players)
            {
                foreach (Player player in players)
                {
                    #region Actions

                    if (player.action.actionID != ActionID.Nothing)
                    {
                        if (player.action.actionID == ActionID.Harvest)
                        {
                               
                            lock (tileDB)
                            {
                             
                               

                                Tile tile;

                                if (tileDB.TryGetValue(((HarvestAction)player.action).tileID, out tile))
                                {
                                    
                                    int newHelpers = ((Field)tileDB[((HarvestAction)player.action).tileID]).helpers;
                                    if (helpers < newHelpers )
                                    {
                                        //TODO cnotrol message: new plater *name* stared helping
                                    }else if (helpers > newHelpers)
                                    {
                                        //TODO control message:  plater *name* stopped helping
                                    }
                                    helpers = newHelpers;
                                   
                                    int foodHarvested = (int)Math.Pow(GameConstants.HARVEST_HELP_MULITPLYER, helpers);
                                    if(helpers != 0)
                                    {
                                        foodHarvested += 1;
                                    }
                                    if (((Field)tile).food <= foodHarvested)
                                    {

                                        foodHarvested = ((Field)tile).food;

                                        ((Field)tileDB[tile.ID]).food = 0;

                                        //stop harvesting
                                        player.action.actionID = ActionID.Nothing;
                                        ((Field)tileDB[((HarvestAction)player.action).tileID]).harvested = false;
                                        
                                         //TODO console meassage: no more food
                                    }
                                    else
                                    {

                                        ((Field)tileDB[tile.ID]).food -= foodHarvested;
                             
                                    }

                                    player.food += foodHarvested;
                                    SMSG_InventoryModified ir = new SMSG_InventoryModified(ItemID.Food, foodHarvested);                                 
                                    ir.sender = "Field_ID" + ((HarvestAction)player.action).tileID.ToString();
                                    SendClient(ir, player.socket);

                                }
                                else
                                {
                                    Console.WriteLine("db: id not found");
                                }
                                    

                            }

                        }
                        else if (player.action.actionID == ActionID.HarvestHelp)
                        {
                           
                        }




                    }else
                    {
                       
                    }
                    #endregion
                }
            }


        }
        static void SecPassed(int waitEatSec)
        {
            if (gameStarted)
            {
                CheckGameTime();
                if((DateTime.Now - lastEatTime).TotalSeconds >= GameConstants.EAT_INERVAL_SEC)
                {
                    Eat();
                    lastEatTime = DateTime.Now;
                }


            }
        }
        static void CheckGameTime()
        {
            if((DateTime.Now - gameStartedTime).TotalSeconds >= GameConstants.GAME_DURATION)
            {
                //game ended
                gameStarted = false;
                SMSG_GameEnded ge = new SMSG_GameEnded(players.Count);
                for(int i = 0; i < players.Count; i++)
                {
                    ge.usernames[i] = players[i].username;
                    ge.scores[i] = players[i].food;
                }

                sendMsgToAllPlayers(ge);
                             
            }
        }

        static void Eat()
        {
            Console.WriteLine("eat ammout: " + eatAmmout * -1);
            sendMsgToAllPlayers(new SMSG_InventoryModified(ItemID.Food, eatAmmout * -1));
         
        }
        static void OnAccept(Object sender)
        {
            //TODO if map loaded = false, send fail and return
            Socket client = (Socket)sender;

            Player player = new Player(client);
            player.ID = lastID++;
            lock (players)
            {
                players.Add(player);
            }


           
            Console.WriteLine("client connected ");


            var e = new SocketAsyncEventArgs();
            e.SetBuffer(new byte[GameConstants.MAX_BYTE_SIZE], 0, GameConstants.MAX_BYTE_SIZE);
            e.Completed += OnData;
            e.UserToken = player;
            client.ReceiveAsync(e);


        }

        static void OnData(object sender, SocketAsyncEventArgs e)
        {
            //Console.WriteLine("data received");
            Player player = ((Player)e.UserToken);
            Socket client = player.socket;
            if (IsSocketDisconnected(client))
            {
                Disconnect(player);
                return;
            }
 
            var e2 = new SocketAsyncEventArgs();
            e2.SetBuffer(new byte[GameConstants.MAX_BYTE_SIZE], 0, GameConstants.MAX_BYTE_SIZE);
            e2.Completed += OnData;
            e2.UserToken = player;
            client.ReceiveAsync(e2);
            
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream ms = new MemoryStream(e.Buffer);
            NetMsg msg = (NetMsg)formatter.Deserialize(ms);
            //Console.WriteLine(msg);

            lock (queuedMessages)
            {
                queuedMessages.Enqueue(new ClientMsg(msg, player));

            }



        }

        private static void ProcessMessage(NetMsg msg, Player player)
        {
            switch (msg.OP)
            {
                case NetOP.CMSG_CreateAccount:
                    {

                        Net_CreateAccount ca = (Net_CreateAccount)msg;
                        player.username = ca.Username;
                        player.passwordHash = ca.Password;

                        Console.WriteLine(player.passwordHash);
                        Console.WriteLine(player.username);

                        break;
                    }
                case NetOP.CMSG_LoginRequest:
                    {
                        Console.WriteLine(player.username);



                        Net_LoginRequest lr = (Net_LoginRequest)msg;
                        player.username = lr.Username;
                        player.passwordHash = lr.Password;


                        SMSG_LoginResponse lres = new SMSG_LoginResponse();
                        lres.successfulLogin = true;
                        lres.playerID = player.ID;

                        Random rnd = new Random(); // only if player logins for the first time
                        int randomPosX = rnd.Next(49, 52);
                        int randomPosY = rnd.Next(49, 52);

                        player.Position = new Vector2(randomPosX, randomPosY);
                        Console.WriteLine("players " + player.ID + " new random cooridnates are " + player.Position.X + ";" + player.Position.Y);

                        SendClient(lres, player.socket);
                        Thread.Sleep(500);

                        SendTileMap();

                        // send other players to me
                        foreach (Player pl in players)
                        {
                            if (pl.ID != player.ID) // dont send me to me in this loop, do it in the other
                            {
                                SMSG_SendPlayer otherPlayerMessage = new SMSG_SendPlayer();
                                otherPlayerMessage.username = pl.username;
                                otherPlayerMessage.playerID = pl.ID;
                                otherPlayerMessage.Position = pl.Position;
                                otherPlayerMessage.moveSpeed = pl.moveSpeed;

                                SendClient(otherPlayerMessage, player.socket);
                            }
                        }

                        // send me to ALL players (including me)
                        SMSG_SendPlayer myPlayerMessage = new SMSG_SendPlayer();
                        myPlayerMessage.username = player.username;
                        myPlayerMessage.playerID = player.ID;
                        myPlayerMessage.Position = player.Position;
                        myPlayerMessage.moveSpeed = player.moveSpeed;

                        sendMsgToAllPlayers(myPlayerMessage);




                        // 
                        break;
                    }
                case NetOP.CMSG_MyDireciton:
                    {

                        CMSG_MyDireciton md = (CMSG_MyDireciton)msg;
                        player.Direction = md.Direction;
                        player.Position = md.Position;
                        player.isDirty = true;
                        
                        break;
                    }
                case NetOP.CMSG_HarvestRequest:
                    {
                        lock (tileDB)
                        {
                            CMSG_HarvestRequest hr = (CMSG_HarvestRequest)msg;

                            if (player.action.actionID == ActionID.Harvest)
                            {
                                // stop harvesting
                                ((Field)tileDB[((HarvestAction)player.action).tileID]).harvested = false;

                                player.action.actionID = ActionID.Nothing;
                                SendClient(new SMSG_ControlMessage(MessageCode.OK, "You have stopped harvesting"), player.socket);
                                sendMsgToAllPlayers(new SMSG_ActionMessage(player.ID, ActionID.Nothing));
                                Console.WriteLine("Player " + player.ID + " stopped harvesting");
                                break;

                            }
                            else if (player.action.actionID == ActionID.Nothing)
                            {
                                Field field = (Field)FindField(player);
                                if (field == null)
                                {
                                 
                                    SendClient(new SMSG_ControlMessage(MessageCode.CantHarvest, "There is no field near you"), player.socket);
                                    sendMsgToAllPlayers(new SMSG_ActionMessage(player.ID, ActionID.Nothing));
                                    break;
                                }
                                else if (field.harvested == true)
                                {
                                  
                                    SendClient(new SMSG_ControlMessage(MessageCode.CantHarvest, "Someone is already harvesting this field"), player.socket);
                                    sendMsgToAllPlayers(new SMSG_ActionMessage(player.ID, ActionID.Nothing));
                                    break;
                                }
                                else if (field.food <= 0)
                                {

                                   
                                    SendClient(new SMSG_ControlMessage(MessageCode.CantHarvest, "There is no more food left"), player.socket);
                                    sendMsgToAllPlayers(new SMSG_ActionMessage(player.ID, ActionID.Nothing));
                                    break;

                                }
                                else
                                {
                                    // start harvesting
                                    HarvestAction a = new HarvestAction(ActionID.Harvest);
                                    a.tileID = field.ID;
                                    player.action = a;

                                    ((Field)tileDB[field.ID]).harvested = true;

                                    SendClient(new SMSG_ControlMessage(MessageCode.OK, "You have started harvesting"), player.socket);
                                    sendMsgToAllPlayers(new SMSG_ActionMessage(player.ID, ActionID.Harvest));
                                    Console.WriteLine("Player " + player.ID + " started harvesting");
                                    break;
                                }
                            }
                            else
                            {
                               
                                SendClient(new SMSG_ControlMessage(MessageCode.CantHarvest, "You must stop doing other actions to harvest"),player.socket);
                            }


                            break;
                        }
                       
                    }
                case NetOP.CMSG_HelpHarvestRequest:
                    {
                        lock (tileDB)
                        {
                           

                            CMSG_HelpHarvestRequest hhr = (CMSG_HelpHarvestRequest)msg;

                            if (player.action.actionID == ActionID.HarvestHelp)
                            {
                                //stop helping
                                player.action.actionID = ActionID.Nothing;

                                ((Field)tileDB[((HelpHarvestAction)player.action).tileID]).helpers--;

                                SendClient(new SMSG_ControlMessage(MessageCode.OK, "You have stopped helping with harvesting the field"), player.socket);
                                sendMsgToAllPlayers(new SMSG_ActionMessage(player.ID, ActionID.Nothing));
                                Console.WriteLine("Player " + player.ID + " stopped helping");
                                break;
                            }
                            else if (player.action.actionID == ActionID.Nothing)
                            {
                                Field field = (Field)FindField(player);

                                if (field == null)
                                {
                              
                                    SendClient(new SMSG_ControlMessage(MessageCode.CantHelp, "There is no field near you"), player.socket);
                                    sendMsgToAllPlayers(new SMSG_ActionMessage(player.ID, ActionID.Nothing));
                                    break;
                                }
                                else if (field.harvested == false)
                                {

                                    
                                    SendClient(new SMSG_ControlMessage(MessageCode.CantHelp, "noone is harvesting this field"), player.socket);
                                    sendMsgToAllPlayers(new SMSG_ActionMessage(player.ID, ActionID.Nothing));
                                    break;

                                }
                                else
                                {
                                    //start helping
                                    HelpHarvestAction a = new HelpHarvestAction(ActionID.HarvestHelp);
                                    a.helper = player.username;
                                    a.tileID = field.ID;
                                    player.action = a;

                                    ((Field)tileDB[field.ID]).helpers++;

                                    SendClient(new SMSG_ControlMessage(MessageCode.OK, "You have started helping wiht harvesting the field"), player.socket);
                                    sendMsgToAllPlayers(new SMSG_ActionMessage(player.ID, ActionID.HarvestHelp));
                                    Console.WriteLine("Player " + player.ID + " started helping");
                                    break;
                                }


                            }
                            else
                            {
                               
                                SendClient(new SMSG_ControlMessage(MessageCode.CantHelp, "You must stop doing other actions to help harvest"), player.socket);
                            }


                            break;
                        }
                   
                    }
                case NetOP.CMSG_StartGame:
                    {
                        gameStarted = true;
                        gameStartedTime = DateTime.Now;
                        lastEatTime = DateTime.Now;
                        int onePlayerGetsFood = (GameConstants.EAT_INERVAL_SEC*1000 / (GameConstants.ACTION_TIME_MILISEC));
                        eatAmmout = onePlayerGetsFood + (int)(players.Count * GameConstants.EAT_DIFFICULTY) +1;
                        //Console.WriteLine("eat ammout: " + eatAmmout);
                  
                        sendMsgToAllPlayers(new SMSG_GameStarted());


                        break;
                    }

            }
        }

        private static void sendMsgToAllPlayers(NetMsg msg)
        {
            lock (players)
            {
                foreach (Player player in players)
                {

                    SendClient(msg, player.socket);


                }

            }

        }

        private static Tile FindField(Player player)
        {
            #region findField
            int tileX = (int)player.Position.X;  //know on which tile player is
            int tileY = (int)player.Position.Y;
        


            if (TileMap[tileY, tileX] == TileID.Field)
            {
              
                foreach (Field t in tileDB.Values) // find the field info in db
                {
                    if (t.posX == tileX && t.posY == tileY)
                    {
                        return t;
                     
                       
                    }
                }

            }
            if (TileMap[tileY + 1, tileX] == TileID.Field && ((Math.Pow(Math.Abs(tileX - player.Position.X), 2) + Math.Pow(Math.Abs(tileY + 1 - player.Position.Y), 2)) < Math.Sqrt(GameConstants.HARVEST_DISTANCE)))
            {
          
                foreach (Field t in tileDB.Values)
                {
                    if (t.posX == tileX && t.posY == tileY + 1)
                    {
                        return t;
                    }
                }
            }
            if (TileMap[tileY, tileX + 1] == TileID.Field && ((Math.Pow(Math.Abs(tileX + 1 - player.Position.X), 2) + Math.Pow(Math.Abs(tileY - player.Position.Y), 2)) < Math.Sqrt(GameConstants.HARVEST_DISTANCE)))
            {
             
                foreach (Field t in tileDB.Values)
                {
                    if (t.posX == tileX + 1 && t.posY == tileY)
                    {
                        return t;
                    }
                }
            }
            if (TileMap[tileY + 1, tileX + 1] == TileID.Field && ((Math.Pow(Math.Abs(tileX + 1 - player.Position.X), 2) + Math.Pow(Math.Abs(tileY + 1 - player.Position.Y), 2)) < Math.Sqrt(GameConstants.HARVEST_DISTANCE)))
            {
         
                foreach (Field t in tileDB.Values)
                {
                  
                    if (t.posX == tileX + 1 && t.posY == tileY + 1)
                    {
                        return t;
                    }
                }
            }
            if (TileMap[tileY - 1, tileX] == TileID.Field && ((Math.Pow(Math.Abs(tileX - player.Position.X), 2) + Math.Pow(Math.Abs(tileY - 1 - player.Position.Y), 2)) < Math.Sqrt(GameConstants.HARVEST_DISTANCE)))
            {
           
                foreach (Field t in tileDB.Values)
                {
                    if (t.posX == tileX && t.posY == tileY - 1)
                    {
                        return t;
                    }
                }
            }
            if (TileMap[tileY, tileX - 1] == TileID.Field && ((Math.Pow(Math.Abs(tileX - 1 - player.Position.X), 2) + Math.Pow(Math.Abs(tileY - player.Position.Y), 2)) < Math.Sqrt(GameConstants.HARVEST_DISTANCE)))
            {
            
                foreach (Field t in tileDB.Values)
                {
                    if (t.posX == tileX - 1 && t.posY == tileY)
                    {
                        return t;
                    }
                }
            }
            if (TileMap[tileY - 1, tileX - 1] == TileID.Field && ((Math.Pow(Math.Abs(tileX - 1 - player.Position.X), 2) + Math.Pow(Math.Abs(tileY - 1 - player.Position.Y), 2)) < Math.Sqrt(GameConstants.HARVEST_DISTANCE)))
            {
             
                foreach (Field t in tileDB.Values)
                {
                    if (t.posX == tileX - 1 && t.posY == tileY - 1)
                    {
                        return t;
                    }
                }
            }
            if (TileMap[tileY + 1, tileX - 1] == TileID.Field && ((Math.Pow(Math.Abs(tileX - 1 - player.Position.X), 2) + Math.Pow(Math.Abs(tileY + 1 - player.Position.Y), 2)) < Math.Sqrt(GameConstants.HARVEST_DISTANCE)))
            {
               
                foreach (Field t in tileDB.Values)
                {
                    if (t.posX == tileX - 1 && t.posY == tileY + 1)
                    {
                            return t;
                           
                    }
                }
            }
            if (TileMap[tileY - 1, tileX + 1] == TileID.Field && ((Math.Pow(Math.Abs(tileX + 1 - player.Position.X), 2) + Math.Pow(Math.Abs(tileY - 1 - player.Position.Y), 2)) < Math.Sqrt(GameConstants.HARVEST_DISTANCE)))
            {
             
                foreach (Field t in tileDB.Values)
                {
                    if (t.posX == tileX + 1 && t.posY == tileY - 1)
                    {
                            return t;
                        }
                }
            }
            return null;
            #endregion
        }

        private static void ResetGame()
        {
            lastID = 0;
            tileDB.Clear();
            CreateMap();
            gameStarted = false;
            
        }

        private static void Disconnect(Player player)
        {
            lock (players)
            {
                Console.WriteLine("Player disconnected " + player.ID);
                players.Remove(player);
                if(players.Count == 0)
                {
                    ResetGame();
                
                }
                sendMsgToAllPlayers(new SMSG_ClientDisconnected(player.ID));
                switch (player.action.actionID)
                {
                    case ActionID.Nothing:
                        {
                            break;
                        }
                    case ActionID.Harvest:
                        {
                            //stop harvesting
                            ((Field)tileDB[((HarvestAction)player.action).tileID]).harvested = false;
                            break;
                        }
                    case ActionID.HarvestHelp:
                        {
                            //stop helping
                            ((Field)tileDB[((HelpHarvestAction)player.action).tileID]).helpers--;
                            break;
                        }
                }
            }
         
        }
            

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Console.WriteLine(e);
        }
        static void SendTileMap()
        {
            for(int y = 0; y < MAP_HEIGHT; y++)
            {
                Thread.Sleep(5);
                SMSG_TileMapRow tmr = new SMSG_TileMapRow(MAP_HEIGHT,MAP_WIDTH, y);
                for (int copyX = 0; copyX < MAP_WIDTH; copyX++)
                {
                    tmr.TileMapRow[copyX] = TileMap[y, copyX];
                }
                sendMsgToAllPlayers(tmr);
            }
         


            
        }

        static bool IsSocketDisconnected(Socket s)
        {                
                        //wait 0.05 sec
            return ((s.Poll(20, SelectMode.SelectRead) && (s.Available == 0)) || !s.Connected);
        
        }

        private static void SendClient(NetMsg msg, Socket playerSocket)
        {
            

            byte[] buffer = new byte[GameConstants.MAX_BYTE_SIZE]; // this is were we hold our data

            //this is where you would cursh your data into a byte[]
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream ms = new MemoryStream(buffer);
            formatter.Serialize(ms, msg);

            var e = new SocketAsyncEventArgs();
            e.SetBuffer(buffer, 0, GameConstants.MAX_BYTE_SIZE);
            playerSocket.SendAsync(e);
          
        }

      
    }
    
}
