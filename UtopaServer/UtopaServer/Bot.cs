﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using UtopiaAPI;

namespace UtopaServer
{
    internal class Bot
    {

        private const int PORT = 26000;
 
        private const string SERVER_IP = "127.0.0.1";
        private Socket serverSocket;
        private byte[] _buffer;
        private int myID;
        float posX = 55;
        float posY = 55;

        public Bot()
        {
        }

        public void Start()
        {
            new Thread(() =>
            {

                serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);


                serverSocket.Connect(IPAddress.Parse("127.0.0.1"), PORT); // 2
                var e = new SocketAsyncEventArgs();
                _buffer = new byte[GameConstants.MAX_BYTE_SIZE];

                e.SetBuffer(_buffer, 0, GameConstants.MAX_BYTE_SIZE);
                e.Completed += OnData;

                serverSocket.ReceiveAsync(e);
                //
                // serverSocket.BeginReceive(_buffer, 0, 1024, 0,
                //     new System.AsyncCallback(Read_Callback), null);
                Net_LoginRequest lr = new Net_LoginRequest();
                lr.Username = "bot1";
                lr.Password = "bot1";
                SendServer(lr);

                Random r = new Random();
                while (true)
                {
                    {
                        CMSG_MyDireciton d = new CMSG_MyDireciton();
                        d.Direction = new UtopiaAPI.Vector2(r.Next(0, 3) - 1, r.Next(0, 3) - 1);
                        d.Position.X = posX;
                        d.Position.Y = posY;
                        SendServer(d);
                        Thread.Sleep(200);
                    }
                    {
                        CMSG_MyDireciton d = new CMSG_MyDireciton();
                        d.Direction = new UtopiaAPI.Vector2(0, 0);
                        d.Position.X = posX;
                        d.Position.Y = posY;
                        SendServer(d);
                        Thread.Sleep(1000);
                    }

                }

            }).Start();

        }

        public void SendServer(NetMsg msg)
        {
            try
            {
                byte[] buffer = new byte[GameConstants.MAX_BYTE_SIZE]; // this is were we hold our data

                //this is where you would cursh your data into a byte[]
                BinaryFormatter formatter = new BinaryFormatter();
                MemoryStream ms = new MemoryStream(buffer);
                formatter.Serialize(ms, msg);


                var e = new SocketAsyncEventArgs();
                e.SetBuffer(buffer, 0, GameConstants.MAX_BYTE_SIZE);
                serverSocket.SendAsync(e);
                //Console.WriteLine("sendeing stared. byte size: " + msg.byteSize);

            }
            catch (Exception e)
            {
                Console.WriteLine("error " + e.ToString());
            }
        }


        private void OnData(object sender, SocketAsyncEventArgs e)
        {
            var socket = (Socket)sender;
            var e2 = new SocketAsyncEventArgs();
            e2.SetBuffer(new byte[GameConstants.MAX_BYTE_SIZE], 0, GameConstants.MAX_BYTE_SIZE);
            e2.Completed += OnData;
            socket.ReceiveAsync(e2);

            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream ms = new MemoryStream(e.Buffer);
            NetMsg msg = (NetMsg)formatter.Deserialize(ms);


            //   GameObject.Find("Debug").GetComponent<UnityEngine.UI.Text>().text += string.Format(" {0}\n", msg);
       
            //Console.WriteLine("Bot " + msg);

            switch (msg.OP)
            {
                case NetOP.SMSG_LoginResponse:
                    {
                        SMSG_LoginResponse lres = (SMSG_LoginResponse)msg;
                        if (lres.successfulLogin)
                        {
                           myID =lres.playerID;

                            Console.WriteLine("Bot id: " + myID);


                        }

                        break;
                    }
                case NetOP.SMSG_PlayersNewCoordinates:
                    {

                        
                        SMSG_PlayersNewCoordinates pnc = (SMSG_PlayersNewCoordinates)msg;
                    
                        if (pnc.id == myID)
                        {



                            posX = pnc.Position.X;
                            posY = pnc.Position.Y;
                          
                        }
                        break;


                    }



            }

        }
    }
}