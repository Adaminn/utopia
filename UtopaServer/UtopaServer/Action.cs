﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtopiaAPI;

namespace UtopaServer
{
    public class Action
    {
        public ActionID actionID;

      

    }
    public class HarvestAction : Action
    {
        //public int helpers = 0;
        public int tileID;

        public HarvestAction(ActionID action) 
        {
            this.actionID = action;
        }

    }
    public class HelpHarvestAction : Action
    {
        public string helper;
        public int tileID;

        public HelpHarvestAction(ActionID action)
        {
            this.actionID = action;
        }

    }
}
