﻿namespace UtopiaAPI
{
    public struct GameConstants
    {
        public const int MAX_BYTE_SIZE = 4096;  // biggest byteSize of net messages

        public const float HARVEST_DISTANCE = 1.45f;
        public const int ACTION_TIME_MILISEC = 500;
        public const float HARVEST_HELP_MULITPLYER = 2f;
        public const int GAME_DURATION = 180;
        public const int EAT_INERVAL_SEC = 5;
        public const float EAT_DIFFICULTY = 0.4f;
        public const int PLAYER_START_FOOD = 100;
    }

    public enum NetOP : byte
    {
        Invalid = 0,
        CMSG_CreateAccount = 1,
        CMSG_LoginRequest = 2,
        SMSG_SendPlayer = 3,
        SMSG_LoginResponse = 4,
        SMSG_PlayersNewCoordinates = 5,
        CMSG_MyDireciton = 6,
        SMSG_TileMapRow = 7,
        CMSG_HarvestRequest= 8,
        SMSG_InventoryModified = 9,
        SMSG_ControlMessage = 10,
        SMSG_ActionMessage = 11,
        CMSG_HelpHarvestRequest = 12,
        CMSG_StartGame = 13,
        SMSG_GameStarted = 14,
        SMSG_GameEnded = 15,
        SMSG_ClientDisconnected = 16
        


    }

    public enum TileID : byte
    {
        Invalid = 0,
        Grass1 = 1,
        Grass2 = 2,
        Field = 3
       

    }

    public enum ActionID : byte
    {
        Nothing = 0,
        Harvest = 1,
        HarvestHelp = 2
       


    }
    public enum ItemID : byte
    {
        Nothing = 0,
        Food = 1
     



    }
    public enum MessageCode : byte
    {
        OK = 0,
        CantHarvest = 1,
        CantHelp = 2,
        ItemRecived = 3




    }

    [System.Serializable]
    public struct Vector2
    {
        public float X;
        public float Y;
        
        public Vector2(float x, float y)
        {
            X = x;
            Y = y;
        }
    }



    [System.Serializable]
    public abstract class NetMsg
    {
        public NetOP OP;
        public int byteSize = 1024;
        public NetMsg()
        {
            OP = NetOP.Invalid;
        }
    }

    [System.Serializable]
    public class Net_CreateAccount : NetMsg
    {
        public Net_CreateAccount()
        {
            OP = NetOP.CMSG_CreateAccount;
        }

        public string Username;
        public string Password; //PasswordHash
    }

    [System.Serializable]
    public class Net_LoginRequest : NetMsg
    {
        public Net_LoginRequest()
        {
            OP = NetOP.CMSG_LoginRequest;
        }

        public override string ToString()
        {
            return $"{nameof(Net_LoginRequest)} {nameof(Username)}: {Username}, {nameof(Password)}: {Password}";
        }

        public string Username;
        public string Password; //PasswordHash
    }

    [System.Serializable]
    public class SMSG_SendPlayer : NetMsg
    {

        public SMSG_SendPlayer()
        {
            OP = NetOP.SMSG_SendPlayer;
        }
       

        public string username;
        public int playerID;
        public float moveSpeed;

        public Vector2 Position;

    }

    [System.Serializable]
    public class SMSG_LoginResponse : NetMsg
    {

        public SMSG_LoginResponse()
        {
            OP = NetOP.SMSG_LoginResponse;
        }


        public bool successfulLogin;
        public int playerID;
       

    }


    [System.Serializable]
    public class SMSG_PlayersNewCoordinates : NetMsg
    {

        public SMSG_PlayersNewCoordinates()
        {
            OP = NetOP.SMSG_PlayersNewCoordinates;


        }

        public Vector2 Position;
        public Vector2 Movement;
        public int id;


    }
    [System.Serializable]
    public class CMSG_MyDireciton : NetMsg
    {

        public CMSG_MyDireciton()
        {
            OP = NetOP.CMSG_MyDireciton;


        }

        public Vector2 Direction;
        public Vector2 Position;

    }
    [System.Serializable]
    public class SMSG_TileMapRow : NetMsg
    {
        public int row;
        public int mapHeight;
        public int mapWidth;
        public TileID[] TileMapRow;
        

        public SMSG_TileMapRow(int mapHeight, int mapWidth, int row)
        {
            OP = NetOP.SMSG_TileMapRow;
            this.mapHeight = mapHeight;
            this.mapWidth = mapWidth;
            TileMapRow = new TileID[ mapWidth];
            this.row = row;
        }


        

    }
   
    [System.Serializable]
    public class CMSG_HarvestRequest : NetMsg
    {

        
         
        public CMSG_HarvestRequest()
        {
            OP = NetOP.CMSG_HarvestRequest;
          
        }




    }
 
    [System.Serializable]
    public class SMSG_InventoryModified : NetMsg
    {
        public ItemID item;
        public int count;
        public string sender;

        public SMSG_InventoryModified(ItemID item, int count)
        {
            OP = NetOP.SMSG_InventoryModified;
            this.item = item;
            this.count = count;
        }

    }


    [System.Serializable]
    public class SMSG_ControlMessage : NetMsg
    {
        public MessageCode messageCode;
        public string textMessage;

        public SMSG_ControlMessage(MessageCode messageCode, string textMessage)
        {
            OP = NetOP.SMSG_ControlMessage;
            this.messageCode = messageCode;
            this.textMessage = textMessage;


        }

    }


    [System.Serializable]
    public class SMSG_ActionMessage : NetMsg
    {
        public ActionID actionID;
        public TileID tileID;
        public int playerID;
        public SMSG_ActionMessage( int plrID, ActionID actID)
        {
            OP = NetOP.SMSG_ActionMessage;
            this.playerID = plrID;
            this.actionID = actID;
            
        }

    }
  

    [System.Serializable]
    public class CMSG_HelpHarvestRequest : NetMsg
    {

       
        public CMSG_HelpHarvestRequest()
        {
            OP = NetOP.CMSG_HelpHarvestRequest;
           
        }

    }
    [System.Serializable]
    public class CMSG_StartGame : NetMsg
    {


        public CMSG_StartGame()
        {
            OP = NetOP.CMSG_StartGame;

        }

    }
    [System.Serializable]
    public class SMSG_GameStarted : NetMsg
    {


        public SMSG_GameStarted()
        {
            OP = NetOP.SMSG_GameStarted;

        }

    }
    [System.Serializable]
    public class SMSG_GameEnded : NetMsg
    {
        public string[] usernames;
        public int[] scores;

        public SMSG_GameEnded(int playersCount)
        {
            OP = NetOP.SMSG_GameEnded;
            this.usernames = new string[playersCount];
            this.scores = new int[playersCount];
        }

    }

    [System.Serializable]
    public class SMSG_ClientDisconnected : NetMsg
    {
      
        public int id;

        public SMSG_ClientDisconnected(int id)
        {
            OP = NetOP.SMSG_ClientDisconnected;
            this.id = id;
          
        }

    }
    




}