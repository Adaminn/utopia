﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UtopiaAPI;

public class GameUIController : MonoBehaviour
{
    void Start()
    {
        GameObject.Find("FoodCount").GetComponent<TMP_Text>().text = "Food: " + GameConstants.PLAYER_START_FOOD;
    }



    public void RefreshFood(int food)
    {
        GameObject.Find("FoodCount").GetComponent<TMP_Text>().text = "Food: " + food;
    }

}

