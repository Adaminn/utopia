﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;


public class LeaderboardController : MonoBehaviour
{
   
 

    public void Display(List<Tuple<string, int>> result)
    {

        string records = "";
        int bigger;
        for (int biggest = result.Count-1; biggest >= 0 && biggest < 5; biggest--)
        {
            for (int j = 0; j < result.Count; j++)
            {
                bigger = 0;
                for (int i = 0; i < result.Count; i++)
                {
                    if (result[j].Item2 > result[i].Item2)
                    {
                        bigger++;
                    }
                }
                if(bigger == biggest)
                {
                    records += (result.Count - biggest) + "# " + result[j].Item1 + " " + result[j].Item2 + "@";
                }
              
            }
        }
        


        records = records.Replace("@",System.Environment.NewLine);
        GameObject.Find("Leaderboard").GetComponent<TMP_Text>().text = records;
    }

    public void GoToMenu()
    {
        StartCoroutine(LoadLogin());
    }


    IEnumerator LoadLogin()
    {
     
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("Login");

        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}

