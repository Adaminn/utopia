﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Diagnostics;
using System.Text;
using UnityEngine;
using UtopiaAPI;

public class InputHandler : MonoBehaviour  // player input
{

 

    private UnityEngine.Vector2 lastMovement;
    private UnityEngine.Vector2 movement;
    private bool harvestKeyPressed;

    
    public ActionID action; 
    

    void Start()
    {
        GameObject camera = GameObject.FindWithTag("MainCamera");
        camera.GetComponent<CameraController>().enabled = true;
        camera.GetComponent<CameraController>().Init();
    }
    void Update()
    {
        // movement
        movement.x = Input.GetAxisRaw("Horizontal"); // from -1 to 1
        movement.y = Input.GetAxisRaw("Vertical"); // from -1 to 1
        movement.Normalize();

        if (action != ActionID.Nothing)
        {
            movement = UnityEngine.Vector2.zero;
        }

        if (movement.x != lastMovement.x || movement.y != lastMovement.y) // if movement changes
        {
            CMSG_MyDireciton md = new CMSG_MyDireciton();
            md.Direction.X = movement.x;
            md.Direction.Y = movement.y;
            md.Position.X = this.transform.position.x;
            md.Position.Y = this.transform.position.y;
            Client.Instance.SendServer(md);
   

         
            if (GetComponent<ApplyMovement>() != null)
            {
                GetComponent<ApplyMovement>().movement = this.movement;
            }
           
        }
        lastMovement = movement;

        //action buttons
        if (Input.GetButtonDown("Harvest") && 
            ((action == ActionID.Nothing) || (action == ActionID.Harvest)) &&  // only if you dont do anything or want to cancel this action
            ((movement == UnityEngine.Vector2.zero) || action == ActionID.Harvest)) //you need to stand still to harvest. this is not required when you want to stop harvesting
        { Harvest(); } 

        if (Input.GetButtonDown("HelpHarvest") && 
            ((action == ActionID.Nothing) || (action == ActionID.HarvestHelp)) && 
            ((movement == UnityEngine.Vector2.zero) || action == ActionID.HarvestHelp))
        { HelpHarvest(); }

        if (Input.GetButtonDown("StartGame")){
            Client.Instance.SendStartGame();
        }
    

    }

    private void Harvest()
    {
        UnityEngine.Debug.Log("Harvest pressed");

        if (action == ActionID.Harvest) // stop harvesting
        {
            action = ActionID.Nothing;
            
          
            Client.Instance.SendHarvest();
        }
        else if (CloseToField())
        {
            action = ActionID.Harvest;
          
          
            Client.Instance.SendHarvest();
        }
       
    }
    private void HelpHarvest()
    {
        UnityEngine.Debug.Log("HelpHarvest pressed");

        if (action == ActionID.HarvestHelp) // stop helping
        {
            action = ActionID.Nothing;
      
          
            Client.Instance.SendHelpHarvest();
        }
        else if (CloseToField())
        {
            action = ActionID.HarvestHelp;
        
          
            Client.Instance.SendHelpHarvest();
        }
       
    }

    private bool CloseToField()
    {
        GameObject[] fields = GameObject.FindGameObjectsWithTag("Field");

        for (int i = 0; i < fields.Length; i++)
        {
           
            if (UnityEngine.Vector2.Distance(new UnityEngine.Vector2(transform.position.x, transform.position.y), new UnityEngine.Vector2(fields[i].transform.position.x, fields[i].transform.position.y)) <= UtopiaAPI.GameConstants.HARVEST_DISTANCE)
            { 
                return true;
            }
            
             
        }

        return false;
    }

}
