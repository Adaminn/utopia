﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Diagnostics;
using System.Text;
using UnityEngine;


public class ApplyMovement : MonoBehaviour
{

    public float moveSpeed = 5f;

    private Animator animator;

    public UnityEngine.Vector2 movement;
    public UnityEngine.Vector2 tempMovement;

    private Stopwatch stopWatch = new Stopwatch();
    private long dTime;

    void Start()
    {
        movement = new Vector2(0, 0);
        animator = GetComponentInChildren<Animator>();
        UnityEngine.Debug.Log("ey3: " + moveSpeed);
    }
    void Update()
    {

        tempMovement = movement;
        if (tempMovement != UnityEngine.Vector2.zero) // because if its 0, blend tree would set sprite to idle_down
        {          
            animator.SetFloat("Horizontal", tempMovement.x);
            animator.SetFloat("Vertical", tempMovement.y);
        }
        animator.SetFloat("Speed", tempMovement.sqrMagnitude);

        dTime = stopWatch.ElapsedMilliseconds;
        //UnityEngine.Debug.Log("deltaTime: " + dTime + "deltaTime: " + Time.deltaTime);
        
      
        this.transform.position = new Vector3(
            this.transform.position.x + tempMovement.x * moveSpeed * Time.deltaTime,
            this.transform.position.y + tempMovement.y * moveSpeed * Time.deltaTime,
            this.transform.position.z);

        // rb.MovePosition(rb.position + movement * 5;
        //UnityEngine.Debug.Log("Moved: " + movement.x * moveSpeed * Time.deltaTime + ";" + movement.y * moveSpeed * Time.deltaTime / 1000);
        stopWatch.Restart();
    }

    void FixedUpdate() //60times per sec
    {

        
    }
}
