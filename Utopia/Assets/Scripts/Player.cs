﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UtopiaAPI;


public class Player
{
    public GameObject gameObject;

    public int? id;
    public string username;
    //List<Item> inventory = new List<Item>();
    public float moveSpeed = 5f;
    public int health;
    public int food;


    public Player(int id)
    {
        this.id = id;
        this.food = GameConstants.PLAYER_START_FOOD;
    }

}
