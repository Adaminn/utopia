﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using UnityEngine;
using UtopiaAPI;
using UnityEngine.SceneManagement;
using TMPro;

public class GameTimer : MonoBehaviour
{

    public bool gameStarted = false;
    private float timePassed = 0;
    public string zero = "";
    private int seconds = 0;

    void Start()
    {
        this.enabled = false;
    }
    void OnEnable()
    {
        timePassed = 0;
    }
    void Update()
    {

        timePassed += Time.deltaTime;

        seconds = Mathf.RoundToInt(60-(timePassed % 60));
        if (seconds > 9)
        {
            zero = "";
        }
        else
        {
            zero = "0";
        }
        GameObject.Find("GameTime").GetComponent<TMP_Text>().text = Mathf.Floor((GameConstants.GAME_DURATION - Mathf.RoundToInt(timePassed)) / 60) + ":"+ zero + seconds;
    }

   
}
