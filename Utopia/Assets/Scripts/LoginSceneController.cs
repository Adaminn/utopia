﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LoginSceneController : MonoBehaviour {

    
	public void OnClickCreateAccount()
    {
        string username = GameObject.Find("CreateUsername").GetComponent<TMP_InputField>().text;
        string password = GameObject.Find("CreatePassword").GetComponent<TMP_InputField>().text;

        Client.Instance.SendCreateAccount(username, password);
    }

    public void OnClickLoginRequest()
    {
     
        if (Client.Instance.gameStarted)
        {
            DisplayMessage("The game has already started. Wait till it ends.");
        }
        else
        {


            string username = GameObject.Find("CreateUsername").GetComponent<TMP_InputField>().text;
            string ip = GameObject.Find("EnterIpInput").GetComponent<TMP_InputField>().text;

            Client.Instance.Init(ip, username);
        }
    }
  
    public void DisplayMessage(string text)
    {
        GameObject.Find("ControlMessage").GetComponent<TMP_Text>().text = text;
    }

}
