﻿using UnityEngine;
using System.Collections;



class CameraController : MonoBehaviour
{

    private GameObject player;

    private Vector3 offset;

    void Awake()
    {
        this.enabled = false;
        this.GetComponent<Camera>().depth = 101;
    }
    void Start()
    {
        player = GameObject.FindWithTag("Player");
     
        offset = new Vector3(0,0,-21);
    }
    public void Init()
    {
       
    }

    void LateUpdate()
    {
        this.transform.position = player.transform.position + offset;
        
    }
}

