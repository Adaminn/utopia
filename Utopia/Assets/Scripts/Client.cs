﻿
using UnityEngine;
using UnityEngine.Networking;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Net;
using System.Net.Sockets;
using UnityEngine.SceneManagement;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Resources;
using System.Threading;
using UtopiaAPI;
using TMPro;


using UnityEditor;
/*
public struct Vector2
{
    public float X;
    public float Y;

    public Vector2(float x, float y)
    {
        X = x;
        Y = y;
    }

    public static implicit operator Vector2(UnityEngine.Vector2 other)
    {
        return new UtopiaAPI.Vector2(other.x, other.y);
    }
    public static implicit operator UtopiaAPI.Vector2(Vector2 other)
    {
        return new UtopiaAPI.Vector2(other.X, other.Y);
    }


}*/

public class Client : MonoBehaviour
{
   
    public static Client Instance { private set; get; }
    
    private const int PORT = 26000;
    
    private const float PIXELS_PER_UNIT = 1f/16f;
  

    private Queue<NetMsg> queuedMessages = new Queue<NetMsg>();

    private Socket serverSocket;
    private static Scene gameScene;
    private bool gameSceneLoaded = false;
    private bool isConnected = false;
    private byte[] _buffer;
    private bool gameSceneIsActive = false;
    private bool connected = false;
    public bool gameStarted = false;

    private Dictionary<int, GameObject> players = new Dictionary<int, GameObject>();
    private static Player me;
   

    void Start()
    {
        Instance = this;
        DontDestroyOnLoad(gameObject);
        GameObject.Find("LoginSceneControl").GetComponent<LoginSceneController>().DisplayMessage("Enter IP, username and click play!");
        Application.targetFrameRate = 60;

        
    }
    public IEnumerator ProcessMessages()
    {
      
        while (true) { 
            while (this.queuedMessages.Count > 0)
            {
              

                lock (this.queuedMessages)
                {
                    var message = queuedMessages.Dequeue();

                    yield return StartCoroutine(ProcessMessage(message));
                }


            }

            yield return null;
        }
        
    }

    private void Update()
    {
        
    }

    public void OnRead(System.IAsyncResult iar)
    {
        
    }

    public void Init(string ip, string username)
    {
     
    
        if(ip == "")
        {
            ip = "127.0.0.1";
        }

        //contect
        Debug.Log(string.Format("Attemtping to connect on {0}", ip));
        serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        serverSocket.Connect(IPAddress.Parse(ip), PORT);
        isConnected = true;

        //recive data    
        var e = new SocketAsyncEventArgs();
        _buffer = new byte[GameConstants.MAX_BYTE_SIZE];

        e.SetBuffer(_buffer, 0, GameConstants.MAX_BYTE_SIZE);
        e.Completed += OnData;

        serverSocket.ReceiveAsync(e);

        //AutomaticLogin();
        SendLoginRequest(username);
        
        StartCoroutine(ProcessMessages());
    }

    private void AutomaticLogin()
    {
        Net_LoginRequest lr = new Net_LoginRequest();
        lr.Username = "a";
        lr.Password = "b";
        SendServer(lr);
    }
    

   
    
    #region OnData

    private void OnData(object sender, SocketAsyncEventArgs e)
    {
        try
        {
            if (!isConnected)
            {
                return;
            }

            var socket = (Socket) sender;
            var e2 = new SocketAsyncEventArgs();
            e2.SetBuffer(new byte[GameConstants.MAX_BYTE_SIZE], 0, GameConstants.MAX_BYTE_SIZE);
            e2.Completed += OnData;
            socket.ReceiveAsync(e2);

            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream ms = new MemoryStream(e.Buffer);
            NetMsg msg = (NetMsg) formatter.Deserialize(ms);
       
            lock (this.queuedMessages)
            {
                this.queuedMessages.Enqueue(msg);

            }
        }
        catch (Exception ex)
        {
            Debug.Log("on data error " + ex.ToString());
        }


    }
    

    /*
    private void LoadMap(SMSG_TileMapRow tm)
    {
  
        GameObject grass1 = Resources.Load<GameObject>("GrassTile1");
        GameObject grass2 = Resources.Load<GameObject>("GrassTile2");
        GameObject field = Resources.Load<GameObject>("FieldTile");

        Debug.Log("is loaded?" + SceneManager.GetActiveScene());
        int height = tm.mapHeight;
        int width = tm.mapWidth;
        Debug.Log("ppu"+PIXELS_PER_UNIT);
        Debug.Log("ppu2" + PIXELS_PER_UNIT * 16 * (-1) + ";"+ PIXELS_PER_UNIT * 16);
  
        //read server tilemap
        for (int y = 0; y < height; y++)
        {
          
            for (int x = 0; x < width ; x++)
            {
            
                //Debug.Log("tilee: " + x + ";" + y + " is "+ tm.TileMap[y,x]);
              
          
                switch (tm.TileMap[y, x])
                {
                    case TileID.Invalid:
                        {
                            Debug.Log("Invalid TileID " );
                           
                            break;
                        }
                    case TileID.Grass1:
                    {
                       
                        Instantiate(grass1, new Vector3(x, y, 0), Quaternion.identity);

                        break;
                    }
                    case TileID.Grass2:
                    {
                      
                        Instantiate(grass2, new Vector3(x, y, 0), Quaternion.identity);

                        break;
                    }
                    case TileID.Field:
                    {
                       
                        Instantiate(field, new Vector3(x, y, 0), Quaternion.identity);

                        break;
                    }

                }
               
            }
        }
    
        
    }
    */



private IEnumerator ProcessMessage(NetMsg msg)
    {
        Debug.Log("Processing " + msg.OP);
        

        switch (msg.OP)
        {
            case NetOP.SMSG_LoginResponse:
            {
                SMSG_LoginResponse lres = (SMSG_LoginResponse)msg;
                if (lres.successfulLogin)
                {
                 
                    me = new Player(lres.playerID);
                           
                    yield return StartCoroutine(LoadGame("Game"));
                    gameSceneLoaded = true;

                }

                break;
            }
            case NetOP.SMSG_TileMapRow:
            {
                    SMSG_TileMapRow tmr = (SMSG_TileMapRow)msg;
                    LoadMapRow(tmr);
              

                break;
            }
            case NetOP.SMSG_SendPlayer:
            {                      
                SMSG_SendPlayer sp = (SMSG_SendPlayer) msg;
                    /*
                                    if (me.id == null)
                                    {

                                        throw new NotSupportedException();

                                    }

                                    string template;

                                    if (me.id == sp.playerID)
                                    {
                                        template = "Player";

                                    }
                                    else
                                    {
                                        template = "OtherPlayer";
                                    }

                                    GameObject g = Resources.Load<GameObject>(template);
                                    Instantiate(g, new Vector3(sp.Position.X, sp.Position.Y, 0), Quaternion.identity);                               
                                    g.GetComponent<ApplyMovement>().moveSpeed = sp.moveSpeed;

                                    if (me.id == sp.playerID)
                                    {
                                        me.gameObject = g;
                                        me.moveSpeed = sp.moveSpeed;
                                    }
                                    else
                                    {
                                        g.GetComponentInChildren<TMP_Text>().text = sp.username;
                                    }

                                    lock (players)
                                    {
                                        players.Add(sp.playerID, g);
                                    }



                                    break;
                                        */
                    if (me.id == null)
                    {
                        throw new NotSupportedException();
                    }

                    string template;
                    Debug.Log("                 sp id:" + sp.playerID);
                    if (me.id == sp.playerID)
                    {
                        template = "Player";

                        GameObject g = Resources.Load<GameObject>(template);
                        g = Instantiate(g, new Vector3(sp.Position.X, sp.Position.Y, 0), Quaternion.identity);
                        me.gameObject = g;
                        me.moveSpeed = sp.moveSpeed;
                        g.GetComponent<ApplyMovement>().moveSpeed = me.moveSpeed;


                    }
                    else
                    {
                        template = "OtherPlayer";


                        GameObject g = Resources.Load<GameObject>(template);
                        g = Instantiate(g, new Vector3(sp.Position.X, sp.Position.Y, 0), Quaternion.identity);
                        g.GetComponent<ApplyMovement>().moveSpeed = sp.moveSpeed;
                        g.GetComponentInChildren<TMP_Text>().text = sp.username;

                        Debug.Log("nametag:" + g.GetComponentInChildren<TMP_Text>().text);
                        players.Add(sp.playerID, g);
                    }



                    break;

                }


            case NetOP.SMSG_PlayersNewCoordinates:
            {
                if (SceneManager.GetActiveScene().name != "Game") // TODO vyresit tak aby nebyl potreba tento if
                {
                    break;
                }

                SMSG_PlayersNewCoordinates pnc = (SMSG_PlayersNewCoordinates)msg;
                GameObject pl;
                lock (players)
                {
                    if (pnc.id != me.id)
                    {

                        if (players.TryGetValue(pnc.id, out pl))
                        {

                            UnityEngine.Vector2 target;
                            target.x = pnc.Position.X;
                            target.y = pnc.Position.Y;

                            pl.transform.position = target;
                       
                            pl.GetComponent<ApplyMovement>().movement.x = pnc.Movement.X;
                            pl.GetComponent<ApplyMovement>().movement.y = pnc.Movement.Y;

                        }
                    }
                    else
                    {
                        //Debug.Log("Server thniksm my position is: " + pnc.Position.X + ";" + pnc.Position.Y);
                    }
                }
              
                break;
            }
            case NetOP.SMSG_InventoryModified:
                {
                    lock (me)
                    {
                        SMSG_InventoryModified im = (SMSG_InventoryModified)msg;

                        

                        if (im.item == ItemID.Food)
                        {

                            me.food += im.count;
                            GameObject.Find("GameController").GetComponent<GameUIController>().RefreshFood(me.food);
                            if (me.food < 0)
                            {
                                Disconnect();
                                yield return StartCoroutine(LoadGame("Login"));
                                GameObject.Find("LoginSceneControl").GetComponent<LoginSceneController>().DisplayMessage("You starved to death. Wait for the next game.");
                            }

                        }
                    }
                    

                    break;
                }
           
            case NetOP.SMSG_ControlMessage:
                {
                    lock (me)
                    {
                        SMSG_ControlMessage cm = (SMSG_ControlMessage)msg;
                        if (cm.messageCode == MessageCode.CantHarvest)
                        {
                            me.gameObject.GetComponent<InputHandler>().action = ActionID.Nothing;
                        }
                        else if (cm.messageCode == MessageCode.CantHelp)
                        {
                            me.gameObject.GetComponent<InputHandler>().action = ActionID.Nothing;
                        }

                        GameObject.Find("GameMessages").GetComponent<TMP_Text>().text = cm.textMessage;
                    }

                    break;
                }
            case NetOP.SMSG_ActionMessage:
                {
                    lock (players)
                    {
                        SMSG_ActionMessage am = (SMSG_ActionMessage)msg;
                        GameObject pl;
                     

                        if (am.playerID != me.id)
                        {
                            pl = players[am.playerID];

                        }else
                        {
                            pl = me.gameObject;
                        
                        }

                        TMP_Text[] gos = pl.GetComponentsInChildren<TMP_Text>();

                        foreach (var ui in gos)
                        {

                            if (ui.name == "CurrentAction")
                            {
                               
                                switch (am.actionID)
                                {
                                    case ActionID.Harvest:
                                        {
                                      
                                            ui.text = "Harvesting...";
                                         
                                            break;
                                        }
                                    case ActionID.HarvestHelp:
                                        {

                                            ui.text = "Helping...";
                                            break;
                                        }
                                    case ActionID.Nothing:
                                        {

                                            ui.text = "";
                                            break;
                                        }
                                }                              


                            }
                        }                            
                        
                    }
                   
                    break;
                }
            case NetOP.SMSG_GameStarted:
                {
                    gameStarted = true;
                    GameObject.Find("GameController").GetComponent<GameTimer>().enabled = true;
                    break;
                }
            case NetOP.SMSG_GameEnded:
                {
                    Disconnect();
                    gameStarted = false;    
                    yield return StartCoroutine(LoadGame("Leaderboard"));

                    SMSG_GameEnded ge = (SMSG_GameEnded)msg;
                    List<Tuple<string, int>> records = new List<Tuple<string, int>>();

                    for (int i = 0; i < ge.scores.Length; i++)
                    {
                        records.Add(Tuple.Create(ge.usernames[i], ge.scores[i]));
                        
                    }
                                  
                    GameObject.Find("UIController").GetComponent<LeaderboardController>().Display(records);

                    break;
                }
            case NetOP.SMSG_ClientDisconnected:
                {
                    lock (players)
                    {
                        GameObject.Find("GameMessages").GetComponent<TMP_Text>().text = "ey";
                        SMSG_ClientDisconnected ge = (SMSG_ClientDisconnected)msg;
                        GameObject pl;
                        if (players.TryGetValue(ge.id, out pl))
                        {

                            GameObject.Find("GameMessages").GetComponent<TMP_Text>().text = "Player disconnected";
                            Destroy(pl);

                        }
                        GameObject.Find("GameMessages").GetComponent<TMP_Text>().text = "eyy";
                        break;
                    }
                
                }


        }
    }


    #endregion

    private void InstantiateOtherPlayer(float posX, float posY)
    {
       
    }
    private void Disconnect()
    {
        isConnected = false;
        SocketAsyncEventArgs e = new SocketAsyncEventArgs();
        serverSocket.DisconnectAsync(e);
    }
    private IEnumerator LoadGame(string name)
    {

        AsyncOperation asyncLoadLevel = SceneManager.LoadSceneAsync(name);
        asyncLoadLevel.allowSceneActivation = true;
        while (!asyncLoadLevel.isDone)
        {
            Debug.Log("loading scene...");

            yield return null;

        }

    }
    #region Send

    private void LoadMapRow(SMSG_TileMapRow tmr)
    {
        //TODO load from a field using tile ID
        GameObject grass1 = Resources.Load<GameObject>("GrassTile1");
        GameObject grass2 = Resources.Load<GameObject>("GrassTile2");
        GameObject field = Resources.Load<GameObject>("FieldTile");

        int height = tmr.mapHeight;
        int width = tmr.mapWidth;
    

        //read server tilemap by rows
       
        for (int x = 0; x < width; x++)
        {

            switch (tmr.TileMapRow[x])
            {
                case TileID.Invalid:
                    {
                        Debug.Log("Invalid TileID ");

                        break;
                    }
                case TileID.Grass1:
                    {

                        Instantiate(grass1, new Vector3(x, tmr.row, 0), Quaternion.identity);

                        break;
                    }
                case TileID.Grass2:
                    {

                        Instantiate(grass2, new Vector3(x, tmr.row, 0), Quaternion.identity);

                        break;
                    }
                case TileID.Field:
                    {

                        Instantiate(field, new Vector3(x, tmr.row, 0), Quaternion.identity);

                        break;
                    }

            }

            
        }

        if (tmr.row == height-1) //loaading completed
        {
            GameObject.Find("GameMessages").GetComponent<TMP_Text>().text = "";
        }

    }

    public void SendServer(NetMsg msg)
    {
        try
        {

        
        byte[] buffer = new byte[GameConstants.MAX_BYTE_SIZE]; // this is were we hold our data
    
        //this is where you would cursh your data into a byte[]
        BinaryFormatter formatter = new BinaryFormatter();
        MemoryStream ms = new MemoryStream(buffer);
        formatter.Serialize(ms, msg);


        var e = new SocketAsyncEventArgs();
        e.SetBuffer(buffer, 0, GameConstants.MAX_BYTE_SIZE);
        serverSocket.SendAsync(e);
       

        }
        catch (Exception e)
        {
            Debug.Log("send error " + e.ToString());
        }
    }

    
    public void SendCreateAccount(string username, string password)
    {
        Net_CreateAccount ca = new Net_CreateAccount();

        ca.Username = username;
        ca.Password = password;


        SendServer(ca);

    }
    public void SendLoginRequest(string username)
    {
        Net_LoginRequest lr = new Net_LoginRequest();

        lr.Username = username;


        SendServer(lr);
    }
    public void SendHarvest()
    {
        SendServer(new CMSG_HarvestRequest());
    }
    public void SendHelpHarvest()
    {
      
  
        SendServer(new CMSG_HelpHarvestRequest());
    }

    public void SendStartGame()
    {
       
        SendServer(new CMSG_StartGame());
    }
    #endregion
   

}

